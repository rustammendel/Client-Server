import java.io.Serializable;


public class User implements Serializable {
    /*
    A User class, which represents the information for a user account, can be serialized, and
    contains:
    o The String attributes username, email, password.
    o A Date attribute called reg_date, initialized to the date the registration is
    happening.
    o An integer called age.
    o Only one constructor that creates the user by using all the aforementioned values.
    o Serialization of this object should not contain the password
    */
    private static final long serialVersionUID = 1L;
    String username;
    String email;
    String password;
    String reg_date;
    int age;

    User(String _uname, String _email, String _password, String _date, int _age) {

        this.username = _uname;
        this.email = _email;
        this.password = _password;
        this.reg_date = _date;
        this.age = _age;
    }

    @Override
    public String toString() {
        return "User [age=" + age + ", email=" + email + ", name=" + username + ", password=" + password + ", reg_date="
                + reg_date + "]";
    }
}