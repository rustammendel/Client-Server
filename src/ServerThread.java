import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class ServerThread extends Thread {
    ArrayList<User> users=new ArrayList<>();
    private Socket s = null;

    public ServerThread(Socket socket) {
        super("ServerThread");
        s = socket;
    }

    public void run() {
        try {
            OutputStream o = s.getOutputStream();
            PrintWriter output = new PrintWriter(o, true);

            //Send Hi to client and wait for answer
            output.println("Hi_client!"); // sent to the client

            //Init IS and wait for input from client
            Scanner input = new Scanner(s.getInputStream());

            input.next(); // ignore hi message from client
            String data = input.next(); // get name from the client
            System.out.println("Connection to client "+ data + " established");

            ObjectInputStream ois=new ObjectInputStream(s.getInputStream());
            User ur = (User) ois.readObject(); //store received user
            System.out.println(ur.toString());

            users.add(ur);
            System.out.println("Fetched user 0 : ");
            System.out.println( users.get(0).toString());


        } catch (IOException e) {
            System.err.println("I/O exception");
            System.exit(1);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                s.close();
            } catch (IOException e) {
                System.err.println("Something went wrong");
                System.exit(2);
            }
        }
    }
}
