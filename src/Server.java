import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/*
* A Server class, which represents a TCP server capable of handling multiple connections at a
time and:
o Contains a dynamic array of users, which is initially empty.
o Listens on port 1234, on localhost;
* */

public class Server {
    private int port = 1234;
    private ServerSocket s = null;

    public static void main(String[] args) throws IOException {
        Server ms = new Server();
        ms.activate();
    }

    public void activate() throws IOException {
        try {
            s = new ServerSocket(port);
        } catch (IOException e) {
            System.err.println("Could not linsten on port " + port);
            System.exit(1);
        }

        while (true) {
            Socket conn = s.accept();
            ServerThread st = new ServerThread(conn);
            st.start();
        }
    }
}
