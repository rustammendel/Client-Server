import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/*A Client class, which represents a client that is able to connect to the aforementioned
server. It contains a String attribute called name, which you should initialise with the same
value you want to use as a username.*/

/*A client connects to a server and the following operations should be performed
o The server sends a “Hi!” string to the client which, after receiving it (should be
checked), prints on standard output the line “Connection to server
established”.

o The client sends a “Hi!” string to the server which, after receiving it (should be
checked), prints on standard output the line “Connection to <client_name>
established”. Of course, this means that the name of the client should also be
sent.
o The client then sends to the server, in order, all the information required to create a
user account.
o The server creates a user, through the information received from the client, stores it
in the array and closes the connection.*/
public class Client {


    public static void main(String[] args) throws Exception {
        Socket s = new Socket("localhost", 1234);
        String name= "tmp";
        User u1 = new User(name,"randatmail.com","helloo123","120299",21);

        try {

            Scanner input = new Scanner(s.getInputStream());
            PrintWriter output = new PrintWriter(s.getOutputStream(), true);
            //wait for input from server and print it
            String data = input.next(); // read data received from server
            System.out.println("Connection to server established"); // do some work on that data
            System.out.println("Message from server: " + data);
            //send hi to server
            output.println("Hi_server!");
            output.println(name);


            ObjectOutputStream os=new ObjectOutputStream(s.getOutputStream());
            os.writeObject(u1);



        } catch (IOException e) {
            System.err.println("Could not connect to host on port ");
            System.exit(1);
        } finally {
            s.close();
        }
    }

}

